��          �       �       �   �  �      �     �  G   �  g   D  �   �  4   �  \   �  �   '  >   �  ;   �  E   #  �   i  c  a     �	      �	  f   �	     a
    �
  R   �  n   O  �   �  M   �  @   �  _   .   Dear User,
You recently clicked the 'Delete Site' link on your site and filled in a
form on that page.
If you really want to delete your site, click the link below. You will not
be asked to confirm again so only click this link if you are absolutely certain:
###URL_DELETE###

If you delete your site, please consider opening a new site here
some time in the future! (But remember your current site and username
are gone forever.)

Thanks for using the site,
Webmaster
###SITE_NAME### Delete My Site Delete My Site Permanently I'm sorry, the link you clicked is stale. Please select another option. I'm sure I want to permanently disable my site, and I am aware I can never get it back or use %s again. If you do not want to use your %s site any more, you can delete it using the form below. When you click <strong>Delete My Site Permanently</strong> you will be sent an email with a link in it. Click on this link to delete your site. Remember, once deleted your site cannot be restored. Thank you for using %s, your site has been deleted. Happy trails to you until we meet again. Thank you. Please check your email for a link to confirm your action. Your site will not be deleted until this link is clicked.  This file is too big. Files must be less than %1$s KB in size. You do not have sufficient permissions to delete this site. You have used your space quota. Please delete files before uploading. PO-Revision-Date: 2012-02-09 22:12:25+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Administration
 Caru Utilizatore,
Venite di cliccà nantu à a leia 'Squassà u situ' di u vostru spaziu è
avete riempiutu a forma di issa pagina.
 S'è vo' vulete propiu squassà u vostru situ, cliccate nantu à a leia quì sottu. Ùn serete
micca avertitu un' altra volta, tandu cliccate nantu à sta leia solu s'è vo' site sicuru sicuru :
###URL_DELETE###

S'è vo' squassate u vostru situ, per piacè pensate à apre ne unu novu quì
da quì à pocu ! (Ma arricurdate vi chì u vostru situ attuale è u vostru cugnome
seranu persi per u sempre).

À ringrazià vi d'avè adupratu u situ,
Maestru di situ
###SITE_NAME###
  Squassà U Mo Situ  Squassà U Mo Situ Per U Sempre  Scusate, ma a leia ch'è vo' avete cliccatu hè sbagliata. Selezziunate un' altra ozzione per piacè.  Sò sicuru di vulè disattivà u mo situ per u sempre, è sò à capu ch'eo ùn u puderaghju mai ricuvarà o aduprà torna %s.  S'è vo' ùn vulete più aduprà u vostru situ %s, u pudete squassà per via di a forma quì sottu. Quand'è vo' cliccate nantu à <strong>Squassà U Mo Situ Per U Sempre</strong> un email vi serà mandatu cun una leia nentru. Cliccate nantu à sta leia per squassà u vostru situ.  Ramentate vi, sguassatu ch'ellu hè, u vostru situ ùn pò esse rimessu in ballu.  À ringrazià vi d'aduprà %s, u vostru situ hè statu squassatu. Bona strada à voi sinu à truvà ci torna.  À ringrazià vi. Per piacè, verificate a vostra scatula email chì ci buscherete a leia per cunfirmà a vostra azzione. U vostru situ ùn serà micca squassatu fin' tantu ch'è vo' ùn averete cliccatu nantu à issa leia.  Sta scheda hè troppu grossa. E schede devenu fà menu di %1$s KO di taglia.  Ùn avete micca e permissione richieste per squassà issu situ.  Avete adupratu tuttu u vostru spaziu. Per piacè, sbarazzate qualchì scheda prima di carigà.  