��    �      �      |      |  D   }     �     �     �  7   	  >   A  Y   �  1   �  &        3     <  '   S  �   {  /     ;   1  F   m  .   �  4   �          (     9     O     [  	   o  	   y  D   �  (   �  P   �  8   B     {     �     �     �  b   �     <  G   Q     �  "   �     �     �  K   �     ?     S  c   a     �  	   �     �     �       (   &     O     _     d     m  
   �     �     �     �  F   �               7  "   J     m     �     �     �     �     �     �     �        �        �     �     �  
   �  %   �  h        k     {     �  1   �  6   �        !   6  !   X  !   z  
   �  
   �  Y   �               ,     >     U     b     o     �     �  /   �     �  +   �  c   &  H   �  5   �     	     '     C     b     s  U   �     �     �  h     w   k  A   �     %      2      D      V      g      {      �      �      �      �   0   �      
!     !  �   !     �!     �!  \   �!     D"     ["     g"  	   }"     �"  G   �"      �"     #  E   %#  H   k#  K   �#  O    $  D   P$  /   �$  :   �$  -    %  =   .%  .   l%  $   �%  @   �%     &  
   &     &     :&     I&     Q&  +   Z&      �&     �&     �&  !   �&  $   '     &'  K   E'     �'  P   �'  b   �'  .   a(  )   �(  9   �(  E   �(  <   :)  "   w)     �)     �)  4   �)     �)     *       *     A*  
   V*     a*     j*     y*  %   �*     �*     �*  )   �*     �*     �*     +     +  O   '+  X   w+  F   �+  5   ,  1   M,  5   ,  5   �,  3   �,  0   -  �   P-  +   �-  �   ".     �.  N   �.  �  /  g   �0  ]    1     ^1     w1     �1  R   �1  }  �1  �   x3  (   4     14     F4     c4  "   4     �4     �4     �4  "   �4  "   5     25     95     N5     ^5  
   n5     y5     �5  "   �5  "   �5     �5      6     6     +6     K6     f6  &   y6  �   �6  U   �7     �7     8     8  F   08  C   w8  w   �8  9   39  5   m9  	   �9     �9  4   �9  �   �9  A   �:  H   �:  R   ';  <   z;  L   �;     <     <  #   6<     Z<     o<     �<     �<  N   �<  5   �<  o   3=  b   �=     >     >     =>  *   P>  �   {>     ?  \   ?  #   |?  -   �?     �?     �?  b   �?  #   a@     �@  g   �@     �@     A     %A  )   DA     nA  .   }A     �A  	   �A  
   �A     �A     �A     B     B     /B  G   =B  #   �B  %   �B  "   �B  *   �B     C      ;C     \C     pC     �C     �C     �C     �C     �C  �   D     �D     �D     �D     �D  2   E  z   4E     �E     �E     �E  <   �E  9   7F     qF     xF     F     �F     �F     �F  a   �F     G     )G     IG  #   aG     �G     �G  /   �G      �G  1   �G  K   +H      wH  *   �H  �   �H  k   YI  9   �I  !   �I  $   !J  $   FJ     kJ  $   �J  m   �J     K     )K  |   9K  �   �K  R   CL     �L     �L     �L     �L  	   �L     �L  
   �L     �L     �L     �L  ?   �L     ;M  
   DM  �   OM     N     &N  h   :N     �N     �N  !   �N     �N  '   �N  N   %O  7   tO  =   �O  R   �O  ]   =P  ]   �P  r   �P  Z   lQ  @   �Q  U   R  =   ^R  U   �R  H   �R  /   ;S  M   kS     �S     �S  +   �S     T      T     .T  +   7T  %   cT  "   �T  )   �T  2   �T  2   	U  (   <U  X   eU  #   �U  U   �U  e   8V  @   �V  0   �V  5   W  G   FW  [   �W  +   �W     X     ,X  C   EX     �X  "   �X  '   �X  !   �X     Y     'Y     5Y      TY  .   uY     �Y     �Y  *   �Y     �Y     Z     Z     Z  U   1Z  \   �Z  K   �Z  A   0[  L   r[  5   �[  B   �[  ?   8\  >   x\  �   �\  2   k]  �   �]     <^  U   Z^    �^  l   �`  k   *a     �a     �a  &   �a  a   �a    Wb  �   gd  3   	e     =e  	   Re     \e     le     se     ze     �e     �e     �e     �e     �e     �e     �e     �e     �e     �e     �e     f     f     f     f  
   f     )f     0f     5f   "caller_get_posts" is deprecated. Use "ignore_sticky_posts" instead. %1$s - Comments on %2$s %1$s and %2$s. %1$s is proudly powered by %2$s %1$s was called <strong>incorrectly</strong>. %2$s %3$s %s comment awaiting moderation %s comments awaiting moderation %s exceeds the maximum upload size for the multi-file uploader when used in your browser. %s exceeds the maximum upload size for this site. &#8220;%s&#8221; has failed to upload. &hellip; &mdash; Select &mdash; (This message was added in version %s.) <a href="http://%1$s">http://%2$s</a> is your new site. <a href="%3$s">Log in</a> as &#8220;%4$s&#8221; using your existing password. <strong>ERROR</strong>: Site URL already taken. <strong>ERROR</strong>: please enter a valid email address. <strong>ERROR</strong>: please fill the required fields (name, email). <strong>ERROR</strong>: please type a comment. <strong>ERROR</strong>: problem creating site entry. About WordPress Add New Category Add New Link Category Add New Tag All Link Categories All Pages All Posts Allow link notifications from other blogs (pingbacks and trackbacks) Allow search engines to index this site. Ambiguous term name used in a hierarchical taxonomy. Please use term ID instead. An error occurred in the upload. Please try again later. Approve and Reply Blockquote (Alt + Shift + Q) Bold (Ctrl + B) Choose from the most used tags Conditional query tags do not work before the query is run. Before then, they always return false. Confirm new password Could not load the preview image. Please reload the page and try again. Create a Configuration File Crop thumbnail to exact dimensions Custom Menu Database Error Destination directory for file streaming does not exist or is not writable. Display as dropdown Documentation ERROR: The themes directory is either empty or doesn&#8217;t exist. Please check your installation. Edit My Profile Edit Site Enter the destination URL Enter your new password below. Entries (RSS) Error establishing a database connection Exit fullscreen F, Y Feedback Front page displays Fullscreen Global Dashboard: %s Help (Alt + Shift + H) Howdy, %1$s If this was a mistake, just ignore this email and nothing will happen. Image default align Image default link type Image default size Insert/edit link (Alt + Shift + A) Invalid attachment ID. Invalid author ID. Invalid post Invalid post format Invalid post type Italic (Ctrl + I) Just write. Large size image height Large size image width Line and paragraph breaks automatic, e-mail address never displayed, <acronym title="Hypertext Markup Language">HTML</acronym> allowed: <code>%s</code> Link Category Link ID Link rating Link title Logged in as <a href="%1$s">%2$s</a>. Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out &raquo;</a> Manage Comments Medium size image height Medium size image width Memory exceeded. Please try another smaller file. Mission complete. Message <strong>%s</strong> deleted. Multisite active signup typeall Multisite active signup typeblog Multisite active signup typenone Multisite active signup typeuser My Account Navigation Need more help? <a href='http://codex.wordpress.org/Editing_wp-config.php'>We got it</a>. Network Admin Network Admin: %s New Category Name New Link Category Name New Tag Name New password No pages found in Trash. No pages found. No posts found in Trash. No search term specified. Showing recent items. Number of links to show: One Response to %2$s %1$s Responses to %2$s One or more database tables are unavailable. The database may need to be <a href="%s">repaired</a>. Only a static class method or function can be used in an uninstall hook. Only lowercase letters (a-z) and numbers are allowed. Open link in a new window/tab Or link to existing content Ordered list (Alt + Shift + O) Parent Category: Parent term does not exist. Passing an integer number of posts is deprecated. Pass an array of arguments instead. Password Reset Permalink: %s Please enter your username or email address. You will receive a link to create a new password via email. Please see <a href="http://codex.wordpress.org/Debugging_in_WordPress">Debugging in WordPress</a> for more information. Please try uploading this file with the %1$sbrowser uploader%2$s. Popular Tags Post formatAside Post formatAudio Post formatChat Post formatGallery Post formatImage Post formatQuote Post formatStandard Post formatStatus Post formatVideo Post types cannot exceed 20 characters in length Poster Preload Read the <a target="_blank" href="http://codex.wordpress.org/Debugging_a_WordPress_Network">bug report</a> page. Some of the guidelines there may help you figure out what went wrong. Refresh Reset Password Scripts and styles should not be registered or enqueued until the %1$s, %2$s, or %3$s hooks. Search Link Categories Search Tags Select Link Category: Shortlink Site Title & Tagline Someone requested that the password be reset for the following account: Sorry, deleting the term failed. Sorry, editing the term failed. Sorry, one of the given taxonomies is not supported by the post type. Sorry, you are not allowed to add a term to one of the given taxonomies. Sorry, you are not allowed to assign a term to one of the given taxonomies. Sorry, you are not allowed to create password protected posts in this post type Sorry, you are not allowed to create private posts in this post type Sorry, you are not allowed to delete this post. Sorry, you are not allowed to edit posts in this post type Sorry, you are not allowed to edit this post. Sorry, you are not allowed to publish posts in this post type Sorry, you are not allowed to stick this post. Sorry, you cannot publish this post. Sorry, your term could not be created. Something wrong happened. Static Front Page Stylesheet Stylesheet is not readable. Support Forums Tagline Template The "%s" theme is not a valid parent theme. The menu ID should not be empty. The passwords do not match. The post cannot be deleted. The post type may not be changed. The post type specified is not valid The term name cannot be empty. The timezone you have entered is not valid. Please select a valid timezone. The user is already active. There are no HTTP transports available which can complete the requested request. There doesn't seem to be a <code>wp-config.php</code> file. I need this before we can get started. This file is not an image. Please try another. This file no longer needs to be included. This is larger than the maximum size. Please try another. This post is password protected. Enter the password to view comments. This taxonomy is not hierarchical so you can't set a parent. This taxonomy is not hierarchical. Thumbnail Height Thumbnail Width To reset your password, visit the following address: Toggle fullscreen mode Unlink (Alt + Shift + S) Unordered list (Alt + Shift + U) Update Link Category Update Tag Updated. Upload failed. Upload stopped. Use <code>WP_User->ID</code> instead. View Category View Tag Whois  : http://whois.arin.net/rest/ip/%s Word count: %s WordPress.org XFN XHTML Friends Network You are currently browsing the <a href="%1$s/">%2$s</a> blog archives for %3$s. You are currently browsing the <a href="%1$s/">%2$s</a> blog archives for the year %3$s. You are currently browsing the <a href="%1$s/">%2$s</a> blog archives. You are not allowed to assign terms in this taxonomy. You are not allowed to create posts as this user. You are not allowed to create terms in this taxonomy. You are not allowed to delete terms in this taxonomy. You are not allowed to edit terms in this taxonomy. You are posting comments too quickly. Slow down. You can create a <code>wp-config.php</code> file through a web interface, but this doesn't work for all server setups. The safest way is to manually create the file. You do not have permission to upload files. You have been added to this site. Please visit the <a href="%s">homepage</a> or <a href="%s">log in</a> using your username and password. You may only upload 1 file. You should specify a nonce action to be verified by using the first parameter. Your account has been activated. You may now <a href="%1$s">log in</a> to the site using your chosen username of &#8220;%2$s&#8221;. Please check your email inbox at %3$s for your password and login instructions. If you do not receive an email, please check your junk or spam folder. If you still do not receive an email within an hour, you can <a href="%4$s">reset your password</a>. Your account is now activated. <a href="%1$s">Log in</a> or go back to the <a href="%2$s">homepage</a>. Your account is now activated. <a href="%1$s">View your site</a> or <a href="%2$s">Log in</a> Your address will be %s. Your latest posts Your password has been reset. Your server is running PHP version %1$s but WordPress %2$s requires at least %3$s. Your site at <a href="%1$s">%2$s</a> is active. You may now log in to your site using your chosen username of &#8220;%3$s&#8221;. Please check your email inbox at %4$s for your password and login instructions. If you do not receive an email, please check your junk or spam folder. If you still do not receive an email within an hour, you can <a href="%5$s">reset your password</a>. Your theme supports %s menu. Select which menu you would like to use. Your theme supports %s menus. Select which menu appears in each location. Your theme supports a static front page. [%1$s] Activate %2$s add new from admin barMedia add new from admin barUser admin bar menu group labelAdd New admin bar menu group labelNew admin color schemeBlue apostrophe&#8217; closing curly double quote&#8221; closing curly single quote&#8217; domain double prime&#8243; em dash&#8212; en dash&#8211; fullscreen http://codex.wordpress.org/ links widgetAll Links opening curly double quote&#8220; opening curly single quote&#8216; prime&#8242; tag delimiter, taxonomy general nameTags taxonomy singular nameCategory taxonomy singular nameTag text directionltr word count: words or characters?words PO-Revision-Date: 2012-03-08 11:22:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/1.0-alpha-1000
Project-Id-Version: Development
 "caller_get_posts" ùn hè più cunsigliatu. Invece, aduprate "ignore_sticky_posts".  %1$s - Cumenti per %2$s  %1$s è %2$s.  %1$s messu in ballu da %2$s  %1$s hè statu chjamatu in modu <strong>sbagliatu</strong>. %2$s %3$s  %s cumentu aspetta un accunsentu %s cumenti aspettanu un accunsentu %s trapassa a taglia massima di carighera per u carigatore multischeda quand'ellu hè adupratu in u vostru navigatore.  %s trapassa a taglia massima di carighera per issu situ.  &#8220;%s&#8221; ùn hà micca riesciutu à carigà.  &hellip;  &mdash; Selezziunà &mdash;  (Stu messagiu hè statu aghjuntu in a versione %s.)  <a href="http://%1$s">http://%2$s</a> is your new site. <a href="%3$s">Identificate vi</a> as &#8220;%4$s&#8221; aduprendu a vostra parolla secreta in usu.  <strong>SBAGLIU</strong>: l'URL di u Situ l'anu digià pigliatu.  <strong>SBAGLIU</strong> : Per piacè, entrite un indirizzu email bonu.  <strong>SBAGLIU</strong> : Per piacè, riempiite u campu richiestu (nome, email).  <strong>SBAGLIU</strong> : Per piacè, scrivite un cumentu.  <strong>SBAGLIU</strong>: Ci hè un prublema per creà l'entrata di u situ.  In quantu à WordPress  Creà una Categuria Nova  Creà una Categuria Nova di Ligami  Creà un Segnu Novu  Tutte e Categurie di Ligami  Tutte e Pagina  Tutti l'Articuli  Accunsentì e nutifiche venendu da altri bloghi (nutifiche ping è retroleie)  Permette à i mutori di ricerca d'indicià stu situ.  Una parolla ambigua hè aduprata in una classifica ghjerarchica. Aduprate invece l'ID di a parolla per piacè.  Ci hè statu un sbagliu durante a incarichera. Pruvate torna da quì à una stundetta per piacè.  Accunsentì è Risponde  Virguletti (Alt + Shift + Q)  Grassu (Ctrl + B)  Sceglie à parte da i segni più aduprati  I termini di richiesta cundiziunale ùn viaghjanu micca prima chì a richiesta fussi messa in ballu. Chì tantu vi vulterà sempre falza.  Cunfirmà a chjave nova  Ùn si pò lancià a prevvista di a fiura. Per piacè rilanciate a pagina è pruvate torna.  Creà una Scheda di Cunfigurazione  Riquadrà a fiuretta à e so dimensione vere  Menù Persunalizatu  Sbagliu di a Basa di Dati  U cartulare di destinazione per a scheda di streaming ùn esiste micca o ùn si pò micca scrive.  Affissà cum'è listinu à sbucinu  Documentazione  SBAGLIU : U cartulare di i tema hè viotu o ùn esiste. Per piacè, verificate a vostra installazione.  Edità u Mo Descrittivu  Edità u Situ  Entrite l'URL di destinazione  Entrite a vostra chjave nova quì sottu.  Entrate (RSS)  Sbagliu fendu a cunnezzione à a basa di dati  Esce da u screniu sanu  F di u Y  Reazzione  A pagina maestra affissa  Screniu sanu  Scagnu Glubale : %s  Aiutu (Alt + Shift + H)  Salute, %1$s  S'ellu hè un sbagliu, ùn date capu à st'email è nunda si passerà.  Alignamentu di a fiura per difettu  Tippu di leia di a fiura per difettu  Dimensione di a fiura per difettu  Inserisce/edità a leia (Alt + Shift + A)  L'ID riligatu hè sbagliatu.  L'ID di l'autore hè sbagliatu.  Articulu sbagliatu  Furmatu d'articulu sbagliatu  Tippu d'articulu sbagliatu  Cursivu (Ctrl + I)  Scrive è basta.  Altura di a fiura maiò  Larghezza di a fiura maiò  I filari è i paragrafi si rompenu automatichi, l'indirizzi e-mail ùn sò mai affissati, <acronym title="Hypertext Markup Language">HTML</acronym> permessu : <code>%s</code>  Categuria di Ligami  ID di a leia  Valutazione di a leia  Titulu di a leia  Cullucatu in quantu chè <a href="%1$s">%2$s</a>.  Cullucatu in quantu chè <a href="%1$s">%2$s</a>. <a href="%3$s" title="Scullucà si da issu contu">Scullucà &raquo;</a>  Gestisce i Cumenti  Altura di a fiura mezana  Larghezza di a fiura mezana  Memoria rebbia. Aduprate per piacè una scheda più chjuca.  Hè fatta. U messagiu <strong>%s</strong> hè squassatu.  tuttu  blogu  nunda  utilizatore  U Mo Contu  Navigazione  Abbisugnate aiutu in più ? <a href='http://codex.wordpress.org/Editing_wp-config.php'>Eccu</a>.  Amministrazione di a Reta  Amministrazione di a Reta : %s  Nome di Categuria Nova  Nome di a Categuria Nova di Ligami  Nome di u Segnu Novu  Chjave nova  Ùn si hè trovu nisuna pagina in a Rumenzula.  Ùn si hè trovu nisuna pagina.  Ùn si hè trovu nisun' articulu in a Rumenzula.  Ùn avete datu nisuna parolla per a ricerca. Eccu l'ultimi elementi messi.  Quantità di leie à affissà :  Una Risposta à %2$s %1$s Risposte à %2$s Una griglia di a basa di dati ùn hè dispunibule è forse ancu parechje. Pò dà si chì a basa di dati abbisogna d'esse <a href="%s">riparata</a>.  Soli un metudu di classa statica o una funzione ponu esse aduprati in un' granchjettu di disinstallazione.  Solu e lettere minuscule (a-z) è i numari sò permessi.  Apre a leia in un' unghjola nova  O ligà à un cuntenutu chì esiste  Listinu in ordine (Alt + Shift + O)  Categuria Parente :  A parolla parente ùn esiste micca.  Ùn hè tantu appreziatu di passà in numari interi di l'articuli. Megliu à passà una matrice d'argumenti.  Riiniziu di a Chjave  Permaleia : %s  Per piacè, intrite u vostru cugnome è u vostru indirizzu email. Riceverete un ligame per creà una chjave nova via email.  Per piacè, vede <a href="http://codex.wordpress.org/Debugging_in_WordPress">Parà i sbagli in WordPress</a> per avè di più infurmazione.  Per piacè, pruvate di carigà sta scheda cun u carigatore%2$s di %1$snavigatore.  Segni Populari  Sviata  Audio  Discursata  Galleria  Fiura  Citazione  Predefinitu  Statutu  Videò  I tippi d'articuli ùn ponu trapassà in 20 caratteri di longu  Affissu  Precarica  Leghje a pagina <a target="_blank" href="http://codex.wordpress.org/Debugging_a_WordPress_Network">bug report</a>. Ci buscherete e direttive chì vi puderanu aiutà à capì ciò chì ùn andava micca.  Attualizà  Riinizià a Chjave  I scritti è e versure ùn devenu esse registrati o infilati nanzu à i granchjetti %1$s, %2$s, o %3$s.  Circà e Categurie di Ligami  Circà Segni  Selezziunà a Caterguria di Leia  Leia corta  Titulu di u situ è descrizzione corta  Qualchissia hà dumandatu à riinizià a so chjave per u contu chì seguita :  Scusate, ci hè statu un sbagliu sguassendu a parolla.  Scusate, ci hè statu un sbagliu à l'edizione di a parolla.  Scusate, una di e classifiche date ùn hè micca adatta à issu tippu d'articuli.  Scusate, ùn avete micca a permissione d'aghjustà una parolla à una di e classifiche date.  Scusate, ùn avete micca a permissione d'appaghjà una parolla à una di e classifiche date.  Scusate, ùn avete micca a permissione di creà articuli prutetti da una parolla secreta in issu tippu d'articuli  Scusate, ùn avete micca a permissione di creà articuli privati in issu tippu d'articuli  Scusate, ùn avete micca a permissione di sguassà st'articulu.  Scusate, ùn avete micca a permissione d'edità l'articuli per issu tippu d'articuli  Scusate, ùn avete micca a permissione d'edità st'articulu.  Scusate, ùn avete micca a permissione di publicà articuli in issu tippu d'articuli  Scusate, ùn avete micca a permissione di mette st'articulu in risaltu.  Scusate, un pudete micca publicà st'articulu.  Scusate, a parolla ùn pò micca esse creata. Hè stalvatu qualchì sbagliu.  Pagina Maestra Statica  Fogliu di Stilu  U fogliu di stilu ùn si pò micca leghje.  Fori da Puntellu  Descrizzione  Vistura  U tema "%s" hè un tema parente sbagliatu.  L'ID di u menù ùn deve esse viotu.  E chjave ùn currispondenu micca.  L'articulu ùn pò micca esse sguassatu.  U tippu d'articulu ùn averebbe da esse cambiatu.  U tippu d'articulu specificatu ùn hè micca bonu  U nome di a parolla ùn pò esse viotu.  A zona di l'ora ch'è vo' avete sceltu ùn hè bona. Selezziunate un' altra per piacè.  L'utilizatore hè digià attivatu.  Ùn ci hè micca trasporti HTTP dispunibuli chì pudessinu compie a richiesta fatta.  Ùn pare micca esse una scheda <code>wp-config.php</code>. Ne aghju bisognu prima di pudè attaccà.  Sta scheda un micca una fiura. Pruvate ne un' altra per piacè.  Sta scheda ùn hà più bisognu d'esse inclusa.  Què trapassa a taglia massima. Pruvate ne un altra.  St'articulu hè prutettu da una chjave. Entrite la per vede i cumenti.  Sta tassunumia ùn hè micca ghjerarchica, tandu ùn li pudete micca appicicà un parente.  Sta tassunumia ùn hè micca ghjerarchica.  Altura di a fiuretta  Larghezza di a fiuretta  Per riinizià a vostra chjave, visitate l'indirizzu chì seguita :  Varcà in modu screniu sanu  Annullà a leia (Alt + Shift + S)  Listinu in disordine (Alt + Shift + U)  Attualizà a Categuria di Ligami  Attualizà u Segnu  Attualizatu.  U incaricamentu hà fiascatu.  U incaricamentu si hè firmatu.  Aduprà <code>WP_User->ID</code> à a piazza.  Vede a Categuria  Vede u Segnu  Whois  : http://whois.arin.net/rest/ip/%s  Numaru di parolle : %s  WordPress.org  XFN  Rete amiche in XHTML  Site in traccia di parcorre l'archivii di u blogu <a href="%1$s/">%2$s</a> per %3$s.  Site in traccia di parcorre l'archivii di u blogu <a href="%1$s/">%2$s</a> per l'annu %3$s.  Site in traccia di parcorre l'archivii di u blogu <a href="%1$s/">%2$s</a>  Ùn avete micca u permessu d'assignà parolle in sta tassunumia.  Ùn avete micca a permissione di creà articuli cun issu statutu di membru.  Avete u permessu di creà parolle in sta tassunumia.  Ùn avete micca u permessu di sguassà parolle in sta tassunumia.  Ùn avete micca u permessu d'edità parolle in sta tassunumia.  I vostri cumenti sò impustati troppu in furia. Andate pianu.  Pudete creà una scheda <code>wp-config.php</code> per via di una interfaccia web, ma ùn viaghja micca cun tutte e cunfigurazione di servori. U megliu hè di creà la à a manu.  Ùn avete micca a permissione d'incaricà schede.  Site statu aghjuntu à u situ. Per piacè andate à l'<a href="%s">iniziu</a> o <a href="%s">cullucate vi</a> aduprendu u vostru cugnome è a vostra chjave.  Pudete mandà 1 scheda sola.  Averestete da specificà un' azzione unica da verificà aduprendu u primu parametru.  U vostru contu hè statu attivatu. Avà v'avereste da pudè <a href="%1$s">cullucà</a> nantu à u situ aduprendu u cugnome ch'è vo' avete sceltu &#8220;%2$s&#8221;. Verificate per piacè a vostra scatula email à %3$s per mette vi à capu di a vostra chjave è di u modu di culluchera. S'è vo' ùn ricevite micca email, attenti di verificà ch'ellu ùn sia statu cunsideratu cum'è impuzzichitu o abusivu. S'è vo' state senza riceve email à capu di un' ora, pudete <a href="%4$s">rinizià a vostra parolla secreta</a>.  Avà u vostru contu hè attivatu. <a href="%1$s">Cullucate vi</a> o vultate à l'<a href="%2$s">iniziu</a>.  Avà u vostru contu hè attivatu. <a href="%1$s">Vede u vostru situ</a> o <a href="%2$s">Cullucate vi</a>.  U vostru indirizzu serà %s.  I vostri ultimi articuli  A vostra chjave hè stata riiniziata.  U vostru servore face girà a versione %1$s di PHP ma WordPress %2$s dumanda à u minimu a %3$s.  U vostru situ à <a href="%1$s">%2$s</a> hè attivu. Avà v'avereste da pudè cullucà nantu à u vostru situ aduprendu u cugnome ch'è vo' avete sceltu di &#8220;%3$s&#8221;. Per piacè, verificate a vostra scatula email à %4$s per mette vi à capu di a vostra chjave è di u modu di culluchera. S'è vo' ùn ricevite micca email, attenti di verificà ch'ellu ùn sia statu cunsideratu cum'è impuzzichitu o abusivu. S'è vo' state senza riceve email à capu di un' ora, pudete <a href="%5$s">rinizià a parolla secreta</a>.  U vostru tema conta %s menù. Selezziunate u menù ch'è vo' vulete aduprà. U vostru tema conta %s menù. Selezziunate chì menù si deve affissà in ogni locu. U vostru tema supporta una pagina maestra statica.  [%1$s] Attivà %2$s  Un Media  Un Utilizatore  Creà  Creà  Azuru  &#8217;  &#8221;  &#8217;  duminiu  &#8243;  &#8212;  &#8211;  screniu sanu  http://codex.wordpress.org/  Tutte e Leie  &#8220;  &#8216;  &#8242;  ,  Segni  Categuria  Segnu  ltr  parolle  